package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest  {
        @Test
        public void testgetCounter() throws Exception {
	int k = new Increment().getCounter();
	assertEquals("Getcounter",1,k);
	}
        @Test
        public void testdecreasecounter() throws Exception {
	int k = new Increment().decreasecounter(1);
	assertEquals("DecreaseGetcounter",1,k);
        }
        @Test
        public void testdecreasecounter1() throws Exception {
        int k = new Increment().decreasecounter(0);
        assertEquals("DecreaseGetcounter",1,k);
        }
        @Test
        public void testdecreasecounter2() throws Exception {
        int k = new Increment().decreasecounter(2);
        assertEquals("DecreaseGetcounter",1,k);
        }
}
